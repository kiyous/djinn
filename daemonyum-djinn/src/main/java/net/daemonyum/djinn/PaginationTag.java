package net.daemonyum.djinn;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * Permet la pagination des élément d'une liste.
 * <d:pagination list="${myList}" subList="mySubList"><c:forEach items="${mySubList}" var="item"> ${item.value1} ${item.value2} </c:forEach><d:pagination>
 * @author Kiyous
 *
 */
@SuppressWarnings("serial")
public class PaginationTag extends TagSupport {

	private static int DEFAULT_NB_ELEMENT = 50;

	private String howMany;
	/* *************** */	
	/* *************** */
	/* tags parameters */
	private List<Object> list;
	private Integer nb;

	private int nbPage;
	private Integer page;
	private String pageParameters;
	private String subListName;


	@Override
	public int doEndTag() throws JspException {
		if(list == null || list.size() == 0) {
			return super.doEndTag();
		}

		if(!(page > nbPage)) {
			try {			
				pageContext.getOut().print("<div id='bottom-paginate'>");

				if(nbPage > 1) {
					pageContext.getOut().print("<div class='pages'>");
					if(page > 1) {
						pageContext.getOut().print("<a href=\"?nb="+nb+"&page=1"+pageParameters+"#topList"+"\">first</a>");
						pageContext.getOut().print("<a href=\"?nb="+nb+"&page="+(page-1)+pageParameters+"#topList"+"\">previous</a>");
					}else {
						pageContext.getOut().print("<span>first</span>");
						pageContext.getOut().print("<span>previous</span>");
					}

					pageContext.getOut().print("<span>"+page+"</span>");

					if(page < nbPage) {
						pageContext.getOut().print("<a href=\"?nb="+nb+"&page="+(page+1)+pageParameters+"#topList"+"\">next</a>");
						pageContext.getOut().print("<a href=\"?nb="+nb+"&page="+(nbPage)+pageParameters+"#topList"+"\">last</a>");
					}else {
						pageContext.getOut().print("<span>next</span>");
						pageContext.getOut().print("<span>last</span>");
					}
					pageContext.getOut().print("</div>");
				}
				pageContext.getOut().print("</div>");
			} catch (IOException e) {
				throw new JspException ("I/O Error", e); 
			}
		}		
		return super.doEndTag();
	}

	@Override
	public int doStartTag() throws JspException {		
		try {
			if(list == null || list.size() == 0) {
				//pageContext.getOut().print("<br/>No result<br/>");
				return SKIP_BODY;
			}

			String pageReq = pageContext.getRequest().getParameter("page");
			String nbReq = pageContext.getRequest().getParameter("nb");

			if(pageContext.getRequest().getAttribute("page") != null) {
				pageReq = (String) pageContext.getRequest().getAttribute("page");
			}
			if(pageContext.getRequest().getAttribute("nb") != null) {
				nbReq = (String) pageContext.getRequest().getAttribute("nb");
			}

			Enumeration<String> params = pageContext.getRequest().getParameterNames();
			pageParameters = "";
			while(params.hasMoreElements()) {
				String param = params.nextElement();
				if(!(param.equals("page")||param.equals("nb"))) {
					pageParameters += "&"+param+"="+pageContext.getRequest().getParameter(param);
				}
			}

			int defaultElementNumber = DEFAULT_NB_ELEMENT;
			try {
				defaultElementNumber = Integer.parseInt(howMany);
			}catch(Exception e) {
				// null ou not a numeric
			}

			nb = nbReq != null ? Integer.parseInt(nbReq) : new Integer(defaultElementNumber);		
			page = pageReq != null ? Integer.parseInt(pageReq) : new Integer(1);		

			double size = list.size();		
			double result = size / nb;		
			nbPage = (int)Math.ceil(result);

			if(page > nbPage) {
				pageContext.getOut().print("<br/>This page does not exist, <a href='?nb="+DEFAULT_NB_ELEMENT+"&page=1"+(pageParameters.trim().length()>0 ? "&"+pageParameters.substring(1) : "")+"'>retry</a><br/>");
				return SKIP_BODY;
			}

			int begin = (page-1) * nb;
			int end = page * nb;

			if(end >= list.size()) {
				end = list.size();
			}

			List<Object> subList = new ArrayList<Object>();
			subList.addAll(list.subList(begin, end));			
			pageContext.setAttribute(subListName, subList);			

			pageContext.getOut().print("<div id='top-paginate'>");
			pageContext.getOut().print("<a name='topList'></a>");
			pageContext.getOut().print("<div class='info'>");
			pageContext.getOut().print("Total: "+list.size() + " - " + nbPage + " pages");
			pageContext.getOut().print("<form action=\"#topList\" method=\"GET\">");

			params = pageContext.getRequest().getParameterNames();
			while(params.hasMoreElements()) {
				String param = params.nextElement();
				if(!(param.equals("page")||param.equals("nb"))) {
					pageContext.getOut().print("<input type='hidden' name='"+param+"' value='"+pageContext.getRequest().getParameter(param)+"' />");
				}
			}

			pageContext.getOut().print(" <label>Number per page: </label><input type='text' name='nb' value='"+nb+"' size='3' maxlength='3' />");
			pageContext.getOut().print(" - ");
			pageContext.getOut().print(" <label>Page: </label><input type='text' name='page' value='"+page+"' size='3' maxlength='3'/>");
			pageContext.getOut().print(" <input class='go' type='submit' value='go' />");
			pageContext.getOut().print("</form>");
			pageContext.getOut().print("</div>");

			if(nbPage > 1) {
				pageContext.getOut().print("<div class='pages'>");
				if(page > 1) {
					pageContext.getOut().print("<a title=\"page 1\" href=\"?nb="+nb+"&page=1"+pageParameters+"#topList"+"\">first</a>");
					pageContext.getOut().print("<a title=\"page "+(page-1)+"\" href=\"?nb="+nb+"&page="+(page-1)+pageParameters+"#topList"+"\">previous</a>");
				}else {
					pageContext.getOut().print("<span>first</span>");
					pageContext.getOut().print("<span>previous</span>");
				}

				pageContext.getOut().print("<span>"+page+"</span>");

				if(page < nbPage) {
					pageContext.getOut().print("<a title=\"page "+(page+1)+"\" href=\"?nb="+nb+"&page="+(page+1)+pageParameters+"#topList"+"\">next</a>");
					pageContext.getOut().print("<a title=\"page "+(nbPage)+"\" href=\"?nb="+nb+"&page="+(nbPage)+pageParameters+"#topList"+"\">last</a>");
				}else {
					pageContext.getOut().print("<span>next</span>");
					pageContext.getOut().print("<span>last</span>");
				}
				pageContext.getOut().print("</div>");
			}
			pageContext.getOut().print("</div>");
			pageContext.getOut().print("<br/>");
		} catch (IOException e) {
			throw new JspException ("I/O Error", e); 
		}

		return Tag.EVAL_BODY_INCLUDE; 
	}

	public void setHowMany(String number) {
		howMany = number;
	}

	public void setList(List<Object> list) { 
		this.list = list;
	}

	public void setSubList(String name) {
		subListName = name;
	}	

}
