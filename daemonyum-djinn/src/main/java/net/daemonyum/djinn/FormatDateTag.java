package net.daemonyum.djinn;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class FormatDateTag extends TagSupport {
	
	private Date date;
	
	private String format;
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public void setFormat(String format) {
		this.format = format;
	}
	
	@Override
	public int doStartTag() throws JspException {
		
		try {
			if(date != null) {
				if(format == null || format.trim().length() == 0) {
					format = "dd/MM/yyyy HH:mm";
				}
				SimpleDateFormat sdf = new SimpleDateFormat(format);		
				pageContext.getOut().print(sdf.format(date));
			}
		} catch (IOException e) {
			throw new JspException ("I/O Error", e); 
		}
		
		return SKIP_BODY;		
	}

}
