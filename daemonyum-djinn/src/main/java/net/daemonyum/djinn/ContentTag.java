package net.daemonyum.djinn;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;

@SuppressWarnings("serial")
public class ContentTag extends TagSupport {
	
	/* *************** */
	/* tags parameters */	
	private String var;
	/* *************** */	
	
	public void setVar(String content) {
		var = content;
	}
	
	@Override
	public int doStartTag() throws JspException {
		
		try {
			pageContext.getOut().print("<jsp:include page=\""+var+"\" />");
		} catch (IOException e) {
			throw new JspException ("I/O Error", e);
		}
		return Tag.SKIP_BODY;
	}

}
